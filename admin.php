<!DOCTYPE html>
<html lang="ru">


	<head> <!-- Техническая информация о документе -->
		<meta charset="UTF-8"> <!-- Определяем кодировку символов документа -->
		<title>P O M O G I T E</title> <!-- Задаем заголовок документа -->
		<link rel="stylesheet" href="style.css">
	</head>

	<body>
		<header><div>
  			<img src="https://i.imgur.com/UxHIhiw.png" width="100"  height = "100" style="float:left; margin-right: 10px" alt="Moon">
			P O M O G I T E</div>

</header>
  <div class="main">
<?php
  if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW'])) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="Enter login and password"');
    print('<h1>401 Требуется авторизация</h1></div></body>');
    exit();
  }

  $user = 'u20401';
  $pass = '3661797';
  $db = new PDO('mysql:host=localhost;dbname=u20401', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  
  $login = trim($_SERVER['PHP_AUTH_USER']);
  $pass_hash = substr(hash("sha256", trim($_SERVER['PHP_AUTH_PW'])), 0, 20);
  $stmtCheck = $db->prepare('SELECT password_admin FROM admin WHERE login_admin = ?');
  $stmtCheck->execute([$login]);
  $row = $stmtCheck->fetch(PDO::FETCH_ASSOC);
  
  if ($row == false || $row['password_admin'] != $pass_hash) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="Invalid login or password"');
    print('<h1>401 Неверный логин или пароль</h1>');
    exit();
  }
?>
    <section>
        <h2>Администрирование</h2>
    </section>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // пароль qwerty
  
  $stmtCount = $db->prepare('SELECT supers, count(fs.id_user) AS amount FROM sups AS sp LEFT JOIN form_supers AS fs ON sp.id_sups = fs.id_sups GROUP BY sp.id_sups');
  $stmtCount->execute();
  print('<section>');
  // print ('Hello');
  // print( $stmtCount->fetch(PDO::FETCH_ASSOC));
  while($row = $stmtCount->fetch(PDO::FETCH_ASSOC)) {
      print('<b>' . $row['supers'] . '</b>: ' . $row['amount'] . '<br/>');
      // print_r($row); 
  }
  print('</section>');

  $stmt1 = $db->prepare('SELECT  id_user, name, email, bd, sex, limbs, bio, login FROM form_data');
  $stmt2 = $db->prepare('SELECT id_sups FROM form_supers WHERE id_user = ?');
  $stmt1->execute();

  while($row = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      print('<section>');
      print('<h2>' . $row['login'] . '</h2>');
      $superpowers = [false, false, false];
      $stmt2->execute([$row['id_user']]);
      while ($superrow = $stmt2->fetch(PDO::FETCH_ASSOC)) {
          $superpowers[$superrow['id_sups'] - 1] = true;
      }
      include('adminform.php');
      print('</section>');
  }
} else {
  if (array_key_exists('delete', $_POST)) {
    $user = 'u20401';
    $pass = '3661797';
    $db = new PDO('mysql:host=localhost;dbname=u20401', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $stmt1 = $db->prepare('DELETE FROM form_supers WHERE  id_user = ?');
    $stmt1->execute([$_POST['uid']]);
    $stmt2 = $db->prepare('DELETE FROM  form_data WHERE id_user = ?');
    $stmt2->execute([$_POST['uid']]);
    header('Location: admin.php');
    exit();
  }
  $errors = FALSE;
  $trimed = [];
  foreach ($_POST as $key => $value)
    if (is_string($value))
      $trimed[$key] = trim($value);
    else
      $trimed[$key] = $value;

  if (empty($trimed['name'])) {
    $errors = TRUE;
  }
  $values['name'] = $trimed['name'];

  if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimed['email'])) {
    $errors = TRUE;
  }
  $values['email'] = $trimed['email'];

  if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimed['bd'])) {
    $errors = TRUE;
  }
  $values['bd'] = $trimed['bd'];

  if (!preg_match('/^[MFO]$/', $trimed['sex'])) {
    $errors = TRUE;
  }
  $values['sex'] = $trimed['sex'];

  if (!preg_match('/^[0-5]$/', $trimed['limbs'])) {
    $errors = TRUE;
  }
  $values['limbs'] = $trimed['limbs'];

  foreach (['1', '2', '3'] as $value) {
    $values['superpowers'][$value] = FALSE;
  }
  if (array_key_exists('superpowers', $trimed)) {
    foreach ($trimed['superpowers'] as $value) {
      print_r($value);
      if (!preg_match('/[1-3]/', $value)) {
        $errors = TRUE;
      }
      $values['superpowers'][$value] = TRUE;
    }
  }
  $values['bio'] = $trimed['bio'];
  

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    print('ERROR');
    //header('Location: admin.php');
    exit();
  }

  $user = 'u20401';
  $pass = '3661797';
  $db = new PDO('mysql:host=localhost;dbname=u20401', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $stmt1 = $db->prepare('UPDATE form_data SET name=?, email=?, bd=?, sex=?, limbs=?, bio=? WHERE id_user = ?');
  $stmt1->execute([$values['name'], $values['email'], $values['bd'], $values['sex'], $values['limbs'], $values['bio'], $_POST['uid']]);

  $stmt2 = $db->prepare('DELETE FROM  form_supers WHERE id_user = ?');
  $stmt2->execute([$_POST['uid']]);

  $stmt3 = $db->prepare("INSERT INTO form_supers SET id_user = ?, id_sups = ?");
  foreach ($trimmedPost['superpowers'] as $s)
    $stmt3 -> execute([$_POST['uid'], $s]);

  header('Location: admin.php');
  exit();
}

?>
</div>
</body>
