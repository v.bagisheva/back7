<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();
$messages = [];
$errors = [];
$trimed=[];

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

  if (!empty($_COOKIE['save'])) {

    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }



  $errors['name'] = !empty($_COOKIE['error_name']);
  $errors['email'] = !empty($_COOKIE['error_email']);
  $errors['bd'] = !empty($_COOKIE['error_bd']);
  $errors['sex'] = !empty($_COOKIE['error_sex']);
  $errors['limbs'] = !empty($_COOKIE['error_limbs']);
  $errors['contract'] = !empty($_COOKIE['error_contract']);

if ($errors['name']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  setcookie('error_name', '', 100000);
  // Выводим сообщение.
  $messages[] = '<div ">Заполните имя.</div>';
}
  

if ($errors['email']) {
  // Удаляем куку, указывая время устаревания в прошлом
  setcookie('error_email', '', 100000);
  // Выводим сообщение.
  $messages[] = '<div ">Заполните почту.</div>';
}
if ($errors['bd']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  setcookie('error_bd', '', 100000);
  // Выводим сообщение.
  $messages[] = '<div ">Заполните др.</div>';
}
if ($errors['sex']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  // Выводим сообщение.
  setcookie('error_sex', '', 100000);
  $messages[] = '<div ">Заполните пол.</div>';
}
if ($errors['limbs']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  // Выводим сообщение.
  setcookie('error_limbs', '', 100000);
  $messages[] = '<div ">Заполните конечности.</div>';
}
if ($errors['contract']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  // Выводим сообщение.
  setcookie('error_contract', '', 100000);
  $messages[] = '<div ">Заполните контракт.</div>';
}

  $values = array();
  $values['name'] = empty($_COOKIE['name_value']) ? '' : strip_tags($_COOKIE['name_value']);
  $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
  $values['bd'] = empty($_COOKIE['bd_value']) ? '' : strip_tags($_COOKIE['bd_value']);
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : strip_tags($_COOKIE['limbs_value']);
  $values['contract'] = empty($_COOKIE['contract_value']) ? '' : strip_tags($_COOKIE['contract_value']);
  $values['superpowers'] = array();
  $values['superpowers'][0] = empty($_COOKIE['superpowers_value_0']) ? '' : $_COOKIE['superpowers_value_0'];
  $values['superpowers'][1] = empty($_COOKIE['superpowers_value_1']) ? '' : $_COOKIE['superpowers_value_1'];
  $values['superpowers'][2] = empty($_COOKIE['superpowers_value_2']) ? '' : $_COOKIE['superpowers_value_2'];

 // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  
  $token='';
  if (!empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) {
    
    $salt = md5(uniqid());
    $token = $salt.':'.substr(hash("sha256",$salt.$_SESSION['secret']),0,20);
    $user = 'u20401';
    $pass = '3661797';
    $db = new PDO('mysql:host=localhost;dbname=u20401', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $stmt1 = $db->prepare('SELECT name, email, bd, sex, limbs, bio FROM  form_data WHERE id_user = ?');
    $stmt1->execute([$_SESSION['uid']]);
    $row = $stmt1->fetch(PDO::FETCH_ASSOC);
    $values['name'] = strip_tags($row['name']);
    $values['email'] = strip_tags($row['email']);
    $values['bd'] = strip_tags($row['bd']);
    $values['sex'] = strip_tags($row['sex']);
    $values['limbs'] = strip_tags($row['limbs']);
    $values['bio'] = strip_tags($row['bio']);

    $stmt2 = $db->prepare('SELECT id_sups FROM form_supers WHERE id_user = ?');
    $stmt2->execute([$_SESSION['uid']]);
    while($row = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      $values['superpowers'][$row['id_sups']-1] = TRUE;
    }
  }

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.


include('form.php');
}

else {
  // Проверяем ошибки.
  foreach ($_POST as $key => $value)
	if (is_string($value))
		$trimed[$key] = strip_tags(trim($value));
	else
		$trimed[$key] = $value;
  $errors = FALSE;
  if ((empty($trimed['name']))) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('error_name', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
  }
  if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimed['email'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('error_email', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }
  
  
  if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimed['bd'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('error_bd', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('bd_value', $_POST['bd'], time() + 30 * 24 * 60 * 60);
  }
  
  if (!preg_match('/^[MFO]$/', $trimed['sex'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('error_sex', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
  }
  if (!preg_match('/^[2-4]$/', $trimed['limbs'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('error_limbs', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
  }
  if (!isset($trimed['contract'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('error_contract', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('contract_value', $_POST['contract'], time() + 30 * 24 * 60 * 60);
  }
  setcookie('superpowers_value_0', '', 100000);
  setcookie('superpowers_value_1', '', 100000);
  setcookie('superpowers_value_2', '', 100000);

  foreach($trimed['superpowers'] as $super) {
    setcookie('superpowers_value_' . ($super-1), 'true', time() + 30 * 24 * 60 * 60);
    }
 
  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('error_name', '', 100000);
    setcookie('error_email', '', 100000);
    setcookie('error_sex', '', 100000);
    setcookie('error_limbs', '', 100000);
    setcookie('error_bd', '', 100000);
    setcookie('error_contract', '', 100000);
  }

    // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
    if (!empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) {
      $salthash = explode(":", $_POST['token']);
      $areyoutoken = $salthash[0].':'.substr(hash("sha256",$salthash[0].$_SESSION['secret']),0,20);
      if($areyoutoken != $_POST['token']) {
        header('Location: https://www.youtube.com/watch?v=1Uzw1Zr1FE4');
        exit();
      }
      

    // TODO: перезаписать данные в БД новыми данными,
    $user = 'u20401';
    $pass = '3661797';
    $db = new PDO('mysql:host=localhost;dbname=u20401', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $stmt1 = $db->prepare('UPDATE form_data SET name=?, email=?, bd=?, sex=?, limbs=?, bio=? WHERE id_user = ?');
    $stmt1->execute([$trimed['name'], $trimed['email'], $trimed['bd'], $trimed['sex'], $trimed['limbs'], $trimed['bio'], $_SESSION['uid']]);

    $stmt2 = $db->prepare('DELETE FROM form_supers WHERE id_user = ?');
    $stmt2->execute([$_SESSION['uid']]);

    $stmt3 = $db->prepare("INSERT INTO form_supers SET id_user = ?, id_sups = ?");
    foreach ($trimed['superpowers'] as $s)
      $stmt3 -> execute([$_SESSION['uid'], $s]);

    }
    else {
      // Генерируем уникальный логин и пароль.
      // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
      $id = uniqid();
      $hash = md5($id);
      $login = substr($hash, 0, 10);
      $pass = substr($hash, 10, 15);
      $pass_hash = substr(hash("sha256", $pass), 0, 20);
      // Сохраняем в Cookies.
      setcookie('login', $login);
      setcookie('pass', $pass);
  
      $user = 'u20401';
      $pass_db = '3661797';
      $db = new PDO('mysql:host=localhost;dbname=u20401', $user, $pass_db, array(PDO::ATTR_PERSISTENT => true));
      $stmt1 = $db->prepare("INSERT INTO form_data SET name = ?, email = ?, bd = ?, 
        sex = ? , limbs = ?, bio = ?, login = ?, pass_hash = ?");
      $stmt1 -> execute([$trimed['name'], $trimed['email'], $trimed['bd'], 
        $trimed['sex'], $trimed['limbs'], $trimed['bio'], $login, $pass_hash]);
      $stmt2 = $db->prepare("INSERT INTO form_supers SET id_user = ?, id_sups = ?");
      $id = $db->lastInsertId();
      foreach ($trimed['superpowers'] as $s)
        $stmt2 -> execute([$id, $s]);
    }
  

  setcookie('save', '1');

  header('Location: ./');
}

